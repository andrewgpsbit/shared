<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call('DefaultSeeder');
		$this->command->info( 'Все таблицы загружены данными!' );

		Model::reguard();
	}

}

class DefaultSeeder extends Seeder {

	public function run()
	{


		DB::table('cities')->insert([
			[ 'name' => 'Краснодар'],
			[ 'name' => 'Ростов-на-дону' ],
			[ 'name' => 'Ставрополь'],
			[ 'name' => 'Сочи'],
			[ 'name' => 'Воронеж'],
			[ 'name' => 'Москва'],
			[ 'name' => 'Питер']
			]);

		DB::table('categories')->insert([
			[ 'name' => 'Быт.техника'],
			[ 'name' => 'Продукты' ],
			[ 'name' => 'Одежда'],
			[ 'name' => 'Обувь'],
			[ 'name' => 'Посуда'],
			]);

		DB::table('products')->insert([
			[ 'name' => 'Утюг', 'category_id' => 1 ],
			[ 'name' => 'Пылесос', 'category_id' => 1 ],
			[ 'name' => 'Колбаса', 'category_id' => 2 ],
			[ 'name' => 'Хлеб', 'category_id' => 2 ],
			[ 'name' => 'Джинсы', 'category_id' => 3 ],
			[ 'name' => 'Рубашка', 'category_id' => 3 ],
			[ 'name' => 'Кросовки', 'category_id' => 4 ],
			[ 'name' => 'Ботинки', 'category_id' => 4 ],
			[ 'name' => 'Тарелка', 'category_id' => 5 ],
			[ 'name' => 'Чайник', 'category_id' => 5 ],
			]);

		DB::table('product_to_cities')->insert([
			[ 'product_id' => 1, 'city_id' => 1 ],
			[ 'product_id' => 2, 'city_id' => 2 ],
			[ 'product_id' => 3, 'city_id' => 3 ],
			[ 'product_id' => 5, 'city_id' => 4 ],
			[ 'product_id' => 6, 'city_id' => 5 ],
			[ 'product_id' => 7, 'city_id' => 6 ],
			[ 'product_id' => 8, 'city_id' => 7 ],
			[ 'product_id' => 9, 'city_id' => 1 ],
			[ 'product_id' => 9, 'city_id' => 2 ],
			[ 'product_id' => 10, 'city_id' => 3 ],
			[ 'product_id' => 10, 'city_id' => 4 ],
		]);


	}

}
