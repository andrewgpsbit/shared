$( document ).ready(function() {

    console.log( "ready!" );

    var btn =  $('.btn-group');
    $('.btn-group .dropdown-toggle').click(function(){
        btn.removeClass('open');
        $(this).parent('.btn-group').addClass('open');
        return false;
    });

    $('body').click(function(){
        btn.removeClass('open');
    })

    $('.btn-group .city').click(function(){
        var category = parseInt( $(this).data('category') );
        $.ajax({
            url: '/index.php/prod/product-ajax/' + category,
            success: function( cities ){
                var li = '';
                $.map( cities,  function( city, b ){
                    li += '<li>'+ city + '</li>'
                });
                $('.cities-' + category).html( li )
            }
        })
    });

});


