@extends('app')

@section('title')
    Моя Нода!!
@endsection
@section('css')
    @parent
    <link href="{{asset('/css/my.css')}}" rel="stylesheet">
@endsection

@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
                {{--@if(isset($status))--}}
                    {{--<div class="alert alert-success">{{$status}}</div>--}}
                {{--@endif--}}
				<div class="panel-heading">Моя нода</div>
				<div class="panel-body">
					<form role="form" enctype="multipart/form-data" method="post" action="{{ action('FirstController@postMytable') }}">
                        <input type="hidden" name="_token" value="{{csrf_token()}}" />
                            <div class="form-group">
                            <label for="name">Введите имя</label>
                            <input type="text" class="form-control" name="name" id="name" placeholder="Введите имя">
                            <p class="help-block">Пример строки с подсказкой</p>
                        </div>
                        <div class="form-group">
                            <label for="body">Введите текст</label>
                            <textarea class="form-control" name="body" id="body" placeholder="Введите текст"></textarea>
                            <p class="help-block">Пример строки с подсказкой</p>
                        </div>

                        <button type="submit" class="btn btn-success">Войти</button>
                    </form>
				</div>
			</div>
				@if(isset($mytable))
				    <div class="panel panel-default">
                        <div class="panel-heading">Вся таблица</div>
                        <div class="panel-body">
                            <table class="table">
                                @foreach($mytable as $row)
                                    <tr>
                                        <td>{{$row->id}}</td>
                                        <td>{{$row->name}}</td>
                                        <td>{{$row->text}}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
				@endif
		</div>
	</div>
</div>
@endsection
