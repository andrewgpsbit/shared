@extends('app')

@section('css')
	@parent
	<link href="{{asset('/css/products.css')}}" rel="stylesheet">
@endsection


@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="panel panel-default">
					<div class="panel-heading">Товары по категориям</div>

					<div class="panel-body">
						<?php if ( Auth::check() ) {
							$email = Auth::user()->email;
							$id = Auth::id();
							print "is logger! $email $id";
						} ?>
							<a href="/user/logout">Выйти</a>
						<div class="box-btn">
							@foreach( $products as $category_id => $product )
								<div class="btn-group">
									<a class="btn city dropdown-toggle" data-category="{{ $category_id }}" href="#">
										Города доставки
										<span class="caret"></span>
									</a><br />
									{{--<a class="btn dropdown-toggle" Переключение данных="dropdown" href="#">--}}
									<span class=""  href="#">
										{{ $product['name'] }} (id {{ $category_id }})
									</span>
									{{--<ul class="dropdown-menu">--}}
									<ul class="cities-{{ $category_id }} dropdown-menu">

									</ul>
									<ul class="menu">
										@foreach( $product['items'] as $item )
											<li><a tabindex="-1" href="#">{{ $item['name'] }} (id {{ $item['id'] }})</a></li>
										@endforeach
									</ul>
								</div>
							@endforeach
						</div>



						<form role="form" enctype="multipart/form-data" method="post" enctype="multipart/form-data" action="{{ action('ProductsController@postAddFile') }}">
						<input type="hidden" name="_token" value="{{csrf_token()}}" />

						<div class="form-group">
							<label for="name">Добавление товаров</label>
							<input type="file" class="form-control" name="file" id="file">
							<p class="help-block">Загрузите txt файл - формата: </p>

							<table class="table table-striped">
								<thead>
								<tr class="danger">
									<th>id Товара</th>
									<th>Название</th>
									<th>id группы</th>
									<th>id городов через запятую</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<th>9;</th>
									<th>Майка;</th>
									<th>3;</th>
									<th>(2,3)</th>
								</tr>
								<tr>
									<td>10;</td>
									<td>Шапка;</td>
									<td>3;</td>
									<td>(1,4)</td>
								</tr>
								</tbody>
							</table>
						</div>

						<button type="submit" class="btn btn-success">Загрузить</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection
