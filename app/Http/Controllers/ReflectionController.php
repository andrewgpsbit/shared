<?php namespace App\Http\Controllers;
use ReflectionClass;

class ReflectionUtil{
  static function getClassSource( ReflectionClass $class ){
    $path = $class->getFileName();
    $lines = @file( $path );
    $from = $class->getStartLine();
    $to = $class->getEndLine();
    $len = $to-$from+1;
    return implode( array_slice( $lines, $from-1, $len ) );
  }
}

class ReflectionController extends Controller{


  public function classData( ReflectionClass $class ){
    $details = '';
    $name = $class->getName();
    if( $class->isUserDefined() ){
      $details .= "$name -- класс определен пользователем<br />";
    }
    if( $class->isInternal() ){
      $details .= "$name -- встроенный класс<br />";
    }
    if( $class->isInterface() ){
      $details .= "$name -- это интерфейс<br />";
    }
    if( $class->isAbstract() ){
      $details .= "$name -- это абстрвктный класс<br />";
    }
    if( $class->isFinal() ){
      $details .= "$name -- это завершенный класс<br />";
    }
    if( $class->isInstantiable() ){
      $details .= "$name -- можно создать экземпляр класса<br />";
    }else{
      $details .= "$name -- нельзя создать экземпляр класса<br />";
    }
    return $details;
  }

  public function methodData( $method ){
    $details = '';
    $name = $method->getName();
    if( $method->isUserDefined() ){
      $details .= "$name -- метод определен пользователем<br />";
    }
    if( $method->isInternal() ){
      $details .= "$name -- внутренний метод<br />";
    }
    if( $method->isAbstract() ){
      $details .= "$name -- это абстрвктный метод<br />";
    }

    if( $method->isPublic() ){
      $details .= "$name -- общедоступный метод<br />";
    }
    if( $method->isProtected() ){
      $details .= "$name -- защищенный метод<br />";
    }
    if( $method->isPrivate() ){
      $details .= "$name -- закрытый метод<br />";
    }

    if( $method->isFinal() ){
      $details .= "$name -- завершенный метод<br />";
    }

    if( $method->isConstructor() ){
      $details .= "$name -- метод конструктора<br />";
    }
    if( $method->returnsReference() ){
      $details .= "$name -- метод возвращает ссылку, а не значеник<br />";
    }
    return $details;
  }

  function argData( \ReflectionParameter $arg ){
    $details = '';
    $declaringClass = $arg->getDeclaringClass();
    $name = $arg->getName();
    $class = $arg->getClass();
    $position = $arg->getPosition();
    $details .= "\$$name находится в позиции $position<br>";
    if( ! empty($class) ){
      $className = $class->getName();
      $details .= "\$$name должен быть обьектом типа $className<br>";
    }
    if( $arg->isPassedByReference() ){
      $details .= "\$$name передан по ссылке<br>";
    }
    if( $arg->isDefaultValueAvailable() ){
      $def = $arg->getDefaultValue();
      $details .= "\$$name по умолчанию равно: $def<br>";
    }
    return $details;
  }

  function getIndex(){
    print '<h4>Reflection</h4>';
    $CDProduct = new CDProduct("Unfogiven", "Группа", "Metallica", 5.55, 5.10);
    print $CDProduct->getSummaryLine() . '<br>';

    //dd(get_declared_classes());
    //dd(get_class($CDProduct));
    //$mumu = CDProduct::$coverUrl;
    //dd(class_implements($test));
    //dd(is_subclass_of($product, 'ShopProduct'));
    //dd(get_parent_class($CDProduct));
    //dd(get_class_vars('CDProduct'));
    //dd(get_class_methods('App\Http\Controllers\CDProduct'));
    //dd(get_class_methods('ReflectionClass'));

    //dd(Reflection::export($prod_class));
    $prod_class = new ReflectionClass('App\Http\Controllers\CDProduct');
    //$prod_class = new ReflectionClass('App\Http\Controllers\Myinterface');
    //$prod_class = new ReflectionClass('App\Http\Controllers\ShopProductWrite2');

    print ReflectionUtil::getClassSource($prod_class);
    //dd( ReflectionUtil::getClassSource($prod_class) );

    //dd($prod_class->getFileName());
    print $this->classData($prod_class);

    $methods = $prod_class->getMethods();
    $method = $prod_class->getMethod('__construct');
    $params = $method->getParameters();
    print '/*********************************************/<br>';
    foreach($params as $param){
      print $this->argData( $param )."<br>";
    }
    print '/*********************************************/';

    // !print ReflectionUtil::getClassSource($prod_class->getMethod('getSummaryLine'));


    foreach($methods as $method){
      //dd(get_class($method));
      //dd($method);
      print $this->methodData($method) . '<br>';
    }

  }
}
