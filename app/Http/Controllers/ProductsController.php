<?php namespace App\Http\Controllers;

use App\Product;
use App\Product_to_city;

use Illuminate\Support\Facades\Input;
use File;


class ProductsController extends Controller{

  function getIndex(){
    return view('products')->withProducts( Product::categoryProducts() );
  }

  // id категории
  function getProductAjax( $id ){
    return Product_to_city::productCities( $id );
  }

  // File add to server and uploadProduct()
  function postAddFile(){
    Product::addFile( Input::file( 'file' ) );
    return redirect()->back();
  }

}