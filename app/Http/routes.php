<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::get('user/logout', function(){
	Auth::logout();
	return redirect()->back();
});


Route::controllers([
	'prod' => 'ProductsController',
	'reflection' => 'ReflectionController',
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
