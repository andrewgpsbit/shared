<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use File;

class Product extends Model {

  public function category()
  {
   $category = $this->hasOne('App\Category', 'id', 'category_id');
    return $category;
  }

  // Возвращает все продукты, по категориям
  public static function categoryProducts()
  {
    $products =  self::with('category')->get();
    $CategoryProducts = [];

    foreach( $products as $item ){
      $id = $item['original']['category_id'];
      $CategoryProducts[ $id ]['name'] = $item['relations']['category']['original']['name'];
      $CategoryProducts[ $id ]['items'][ $item['original']['id'] ] = $item['original'];
    }
    return $CategoryProducts;
  }

  // Upload products
  public static function uploadProduct( $products )
  {
    foreach( $products as $product ){
      $prodObj = new Product;
      $prodObj->id = $product['id'];
      $prodObj->name = $product['name'];
      $prodObj->category_id = $product['category_id'];
      $prodObj->save();
      foreach( $product['cities'] as $city ){
        $cityObj = new Product_to_city;
        $cityObj->product_id = $product['id'];
        $cityObj->city_id = $city;
        $cityObj->save();
      }
    }
  }

  // File add to server and uploadProduct()
  public static function addFile( $sendFile )
  {
    $sendFile->move( 'txt/' , 'products.txt');

    $file =  file( public_path() . '/txt/products.txt');

    $products = [];
    foreach( $file as $key => $row ){

      $row =  explode(";", $row);
      $cityStr = str_replace( [ '(' , ')', '/n'  ], "", $row[3] );

      $cityArray = [];
      foreach ( explode(",", $cityStr) as $city_id ){
        $cityArray[] = intval( $city_id );
      }
      $products[ $key ]['id'] = intval( $row[0] );
      $products[ $key ]['name'] = $row[1];
      $products[ $key ]['category_id'] = intval( $row[2] );
      $products[ $key ]['cities'] = $cityArray;

    }
    self::uploadProduct( $products );

  }


}


