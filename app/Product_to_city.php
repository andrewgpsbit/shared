<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_to_city extends Model {

  protected $table = 'product_to_cities';

  public function city()
  {
    $category = $this->belongsTo('App\City', 'city_id', 'id');
    return $category;
  }

  // id категории
  public static function productCities( $id )
  {
    // массив id продуктов данной категории
    $productFromCategory = array_keys( Product::categoryProducts()[ $id ]['items'] );
    // Таблица городов у продуктов
    $productsCity =  self::with('city')->get();

    $currentCities = [];
    foreach( $productsCity as $product ){
      // Если есть город доставки для продукта из данной категории
      if( in_array( $product['original']['product_id'], $productFromCategory ) ){
        $currentCities[ $product['relations']['city']['original']['id'] ] = $product['relations']['city']['original']['name'];
      }
    }

    return $currentCities;
  }


}
