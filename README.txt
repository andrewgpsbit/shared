ТЕХНИЧЕСКОЕ ЗАДАНИЕ:

Имеется следующие данные:
- категории
- товары
- города возможной доставки
Товар принадлежит только к одной категории и может иметь одновременно несколько городов
для доставки.
Реализовать:



по вашему представлению, оптимальную структуру БД со связями.
Страницу на которой выводятся товары разбитые по категориям. К каждой категории
сделать контрол, по клику на который аяксом будут подгружены все города, куда могут
быть доставлены товары данной категории.
Возможность (на той же или на другой странице, не важно) импорта товаров. Формат
файла на ваше усмотрение.







РЕАЛИЗАЦИЯ:

Затраченое время около 15 часов.

Использовал Laravel


Таблицы создавал через ORM красноречивую php artisan make:model
Добавлял к моделям таблиц функции для получения полей связанных таблиц (foreign cascade)

Если на голом SQL то структура получилась такая:

CREATE TABLE category (
    id integer auto_increment not null primary key,
    name varchar(100) not null
);

CREATE TABLE city (
    id integer auto_increment not null primary key,
    name varchar(100) not null
);

CREATE TABLE product (
    id integer auto_increment not null primary key,
    name varchar(100) not null,
    category_id integer,
    foreign key (category_id) references category(id)
    	on update CASCADE
        ON delete restrict
);


CREATE TABLE product_to_city (
    product_id integer not null,
    city_id integer not null,
    foreign key (product_id) references product(id)
    	on update cascade
        ON delete cascade,
    foreign key (city_id) references city(id)
    	on update cascade
        ON delete cascade
);



Все остальное происходит в ProductsController

Формат файла хотел сделать xls, но захотел спать, поэтому получилось txt

Что бы все заработало необходимо добавить в таблицы названия: Категорий и Городов,
после чего можно будет добавлять продукты через txt файл
И просматривать по клику "Города доставки" через ajax


P.S. Логически у меня в голове все сложилось быстро, терял время ORM красноречивую и
мой комп начинает тормозить когда работаешь на linux + phpstorm + chrome ( 2g ram моловато )

Второй раз на нормальном компе, конечно намного быстрее будет.

Было очень интересно, спасибо вам за задание, время пролетело незаметно.